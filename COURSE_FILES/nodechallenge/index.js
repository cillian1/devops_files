const mongoclient = require("mongodb")
const express = require('express');
const app = express();
app.set('view engine', 'ejs');
const body_parser = require('body-parser')
const urlencoded_parser = body_parser.urlencoded({ extended: true});
const db = mongoclient.MongoClient
const url = "mongodb://localhost:27017/test";

mongoclient.connect(url, { useNewUrlParser: true }, function(err, db) {
  if (err) throw err;
    var dbo = db.db("aggdemo");

    console.log("Connected");

});
app.get('/',(req,res)=>{
  res.sendFile(__dirname+'/index.html');
  console.log(__dirname);
});



app.get('/range',(req,res)=>{
  res.render('rangeFind.ejs')
});

app.post('/range',urlencoded_parser,(req,res)=>{
mongoclient.connect(url, { useNewUrlParser: true }, function(err, db){
var dbo = db.db("test");
var quantity1 = parseInt(req.body.q1);
var quantity2 = parseInt(req.body.q2);
var query = {Quantity:{$gte:quantity1,$lte:quantity2}}

dbo.collection("aggdemo").find(query).toArray(function(err, result) {
    if (err) throw err;
    console.log(result);
    res.render('rangeDisplay.ejs',{products:result});
  //res.render('rangeFind.ejs')
  });
});
});
app.get('/product',(req,res)=>{
  res.render('getproduct.ejs')
});

app.post('/product',urlencoded_parser,(req,res)=>{
  mongoclient.connect(url, { useNewUrlParser: true }, function(err, db) {
    if (err) throw err;
    var dbo = db.db("test");
    var category = req.body.category;
    var year = parseInt(req.body.year);
    var quantity = parseInt(req.body.quantity);
    dbo.collection("aggdemo").find({Category:category, Year: year, Quantity: {$gte:quantity}}).toArray(function(err, result) {
      if (err) throw err;
      console.log(result);
      res.render('products.ejs',{products:result});
      db.close();
    });
  });
});






app.get('/',(req,res)=>{
  res.sendFile(__dirname+'/index.html');
  console.log(__dirname);
});

app.listen(8081);
console.log("Server listening on port 8081");
