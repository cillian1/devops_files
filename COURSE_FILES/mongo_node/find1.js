const mongoclient = require("mongodb")
const db = mongoclient.MongoClient
const url = "mongodb://localhost:27017/test";
mongoclient.connect(url, { useNewUrlParser: true }, function(err, db) {
  if (err) throw err;
  var dbo = db.db("test");
  dbo.collection("contacts").findOne({"age":26}, function(err, result) {
    if (err) throw err;
    console.log(result.name);
    db.close();
  });
});
