const mongoclient = require("mongodb");
const db = mongoclient.MongoClient;
const url = "mongodb://localhost:27017";
mongoclient.connect(url, { useNewUrlParser: true }, function(err, db) {
  if (err) throw err;
  var dbo = db.db("test");
  dbo.collection("demo").find({"Price":{$gte:50}}).toArray(function(err, result) {
    if (err) throw err;
    console.log(result);
    db.close();
  });
});
