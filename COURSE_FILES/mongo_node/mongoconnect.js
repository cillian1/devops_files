const mongoclient = require("mongodb")
const db = mongoclient.MongoClient
const url = "mongodb://localhost:27017/test";
mongoclient.connect(url, { useNewUrlParser: true }, function(err, db) {
  if (err) throw err;
  var dbo = db.db("test");
  var myobj = { name: "Sue Smith", age: 26 };
  dbo.collection("contacts").insertOne(myobj, function(err, res) {
    if (err) throw err;
    console.log("1 document inserted");
    db.close();
  });
});
