const express = require('express');
const app = express();
const body_parser = require('body-parser')
const urlencoded_parser = body_parser.urlencoded({ extended: true});
const mysql = require('mysql');
const con = mysql.createConnection({
  host: "localhost",
  user: 'root',
  password: "c0nygre",
  database: "courseDB"
});

app.get('/',(req,res)=>{
  res.sendFile(__dirname+'/index.html');
  console.log(__dirname);
});

app.get('/contacts',(req,res)=>{
  res.send('Recieved a request for a list of contacts')
});

app.get('/addcontact',(req,res)=>{
  res.sendFile(__dirname+'/addcontact.html');

});
app.post('/new',urlencoded_parser,(req,res)=>{
  con.connect(function(err) {
	  if (err) throw err;
	  console.log("Connected!");
	  var sql = "INSERT INTO contacts(first_name,last_name,phone) VALUES(?,?,?)";
	  con.query(sql,[req.body.fname,req.body.lname,req.body.phone],function (err, result) {
	    if (err) throw err;
	    console.log("1 record inserted");
	  });
	});
  res.send(req.body.fname + ' ' + req.body.lname + '<br />'+
  'Phone: '+req.body.phone +' details added to the database');
});

app.get('/new',(req,res)=>{
  res.send('Hello' + req.query.fname + '' + req.query.lname + '<br />'+
  'Phone: '+req.query.phone);
});

app.listen(8081);
console.log("Server listening on port 8081");
