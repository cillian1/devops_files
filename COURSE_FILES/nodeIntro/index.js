const express = require('express');
const app = express();
app.set('view engine', 'ejs');
const body_parser = require('body-parser')
const urlencoded_parser = body_parser.urlencoded({ extended: true});
const mysql = require('mysql');
const con = mysql.createConnection({
  host: "localhost",
  user: 'root',
  password: "c0nygre",
  database: "courseDB"
});

con.connect(function(err) {
  if (err) throw err;
  console.log("Connected");
});

app.get('/contacts',(req,res)=>{
  var sql = "SELECT first_name, last_name, phone, id FROM contacts";
  con.query(sql,function (err, result) {
    if (err) throw err;
    console.log(result);
    res.render('contacts.ejs',{contacts:result})
  });
});
app.post('/update',urlencoded_parser,(req,res)=>{
	  var sql = "UPDATE contacts SET first_name=?,last_name=?,phone=?"+" WHERE id = ?";
	  con.query(sql,[req.body.fname,req.body.lname,req.body.phone,req.body.contactid],function (err, result) {
	    if (err) throw err;
	    console.log("1 record updated");
	  });
  console.log(req.body.fname + ' ' + req.body.lname + '<br />'+
  'Phone: '+req.body.phone +' details changed in the database');
  res.sendFile(__dirname + '/index.html');
});

app.get('/delete',(req,res)=>{
  var sql = "DELETE FROM contacts WHERE id = ?";
  con.query(sql,[req.query.id],function (err, result) {
    if (err) throw err;
    console.log("1 record deleted");
  });
  //console.log(req.body.fname + ' ' + req.body.lname + '<br />'+
//res.sendFile(__dirname + '/index.html')
//res.sendFile(__dirname + '/contacts.ejs');
res.render('contacts.ejs')
})


app.get('/edit',(req,res)=>{
  var sql = "SELECT first_name, last_name, phone, id FROM contacts" +" WHERE id = ?";
  con.query(sql,[req.query.id],function (err, result) {
    if (err) throw err;
    console.log(result);
    res.render('editcontact.ejs',{contact:result})
  });

});


app.get('/',(req,res)=>{
  res.sendFile(__dirname+'/index.html');
  console.log(__dirname);
});

/*app.get('/contacts',(req,res)=>{
  res.send('Recieved a request for a list of contacts')
});*/

app.get('/addcontact',(req,res)=>{
  res.sendFile(__dirname+'/addcontact.html');

});
app.post('/new',urlencoded_parser,(req,res)=>{
	  var sql = "INSERT INTO contacts(first_name,last_name,phone) VALUES(?,?,?)";
	  con.query(sql,[req.body.fname,req.body.lname,req.body.phone],function (err, result) {
	    if (err) throw err;
	    console.log("1 record inserted");
	  });
//  res.send(req.body.fname + ' ' + req.body.lname + '<br />'+
  //'Phone: '+req.body.phone +' details added to the database');
  res.sendFile(__dirname + '/index.html');

});

app.get('/new',(req,res)=>{
   res.send('Hello' + req.query.fname + '' + req.query.lname + '<br />'+
  'Phone: '+req.query.phone);
  res.sendFile(__dirname + '/index.html')
});

app.listen(8081);
console.log("Server listening on port 8081");
