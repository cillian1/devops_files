const chai = require('chai');
const expect = chai.expect;
const assert = chai.assert;
const animals = {pets:['dog','cat','hamster']};
const foo = 'bar';

function mysquare(n){
  return n;
}

function fizzbuzz(n){
  if((n % 3===0 )&& (n % 5===0)){
    return "fizzbuzz";
  }
  else if((n % 3===0)){
    return "fizz";
  }
  else if((n % 5===0)){
    return "buzz";
  }
  else{
    return n;
  }
}

describe('Some code to test', ()=>{
  it('My First Test',()=>{
    const anumber = 10;
    expect(anumber).to.exist
  });
  it('This is another test',()=>{
    const val = false;
    expect(val).to.equal(false);
  });


  it('Test fizzbuzz',()=>{
    var req = fizzbuzz(15);
    expect(req).to.equal("fizzbuzz");

    var req1 = fizzbuzz(3);
    expect(req1).to.equal("fizz");

    var req2 = fizzbuzz(5);
    expect(req2).to.equal("buzz");

    var req3 = fizzbuzz(4);
    expect(req3).to.equal(4);
    var req2 = fizzbuzz(45);
    expect(req2).to.equal("fizzbuzz");
  });
});
// afterEach(()=>{
//
// });
// it('This is a test', ()=>{
//   const num = 1;
//   expect(num).to.exist;
// });
