let mongoose = require("mongoose");
let Contact = require('../api/models/contactsModel');

let chai = require('chai');
let chaiHttp = require('chai-http');
let server = require('../index');
let should = chai.should();
chai.use(chaiHttp);

describe('/GET contacts', () => {
      it('it should GET all the contacts', (done) => {
        chai.request(server)
            .get('/contacts')
            .end((err, res) => {
                  res.should.have.status(200);
                  res.body.should.be.a('array');
              done();
            });
      });

      describe('/POST add a contact', () => {
          it('Post a contact', (done) => {
              let contact = {
                  name: "Joe Tester",
                  phone: "00353 1234567"
              }
              chai.request(server)
                .post('/contacts')
                .send(contact)
                .end((err, res) => {
                      res.should.have.status(200);
                      res.body.should.be.a('object');
                  done();
                });
          });

      });
      describe('/PUT change a contact', () => {
          it('PUT a contact', (done) => {
              let contact = {
                  name: "Tedwell Bundy",
                  phone: "00353 1234567"
              }
              chai.request(server)
                .put('/contacts/5c62b57ffb9558123812a32a')
                .send(contact)
                .end((err, res) => {
                      res.should.have.status(200);
                      res.body.should.be.a('object');
                        res.body.should.have.property('name').eq('Tedwell Bundy')
                  done();
                });
          });

      });
      describe('/DELETE  a contact', () => {
          it('DELETE a contact', (done) => {
              chai.request(server)
                .delete('/contacts/5c618dc33c53d216745a4ba9')
                .end((err, res) => {
                  res.should.have.status(200);
                  done();
                });
          });

      });




  });
