const express = require('express');
const app = express();
const body_parser = require('body-parser')
const urlencoded_parser = body_parser.urlencoded({ extended: true
});
const mongoclient = require("mongodb");

const db = mongoclient.MongoClient;
const url = "mongodb://localhost:27017/test";

app.set('view engine','ejs');

mongoclient.connect(url, { useNewUrlParser: true }, function(err, db) {
  if (err) throw err;
  console.log("Connected");
  });

app.get('/',(req,res)=>{

    res.sendFile(__dirname + '/public/index.html');
});
app.get('/index',(req,res)=>{

    res.sendFile(__dirname + '/public/index.html');
});

app.get('/getcontinentlist',(req,res)=>{
	var sql = "SELECT DISTINCT continent from country";
	  con.query(sql,function (err, result) {
	    if (err) throw err;
			console.log(result);
			res.send(result);
	  });
});



app.get('/getcountrylistb',(req,res)=>{
	var sql = "SELECT name, code from country WHERE continent = ?";
	  con.query(sql,[req.query.continent],function (err, result) {
	    if (err) throw err;
			console.log(result);
			res.send(result);
	  });
});















app.post('/getyearlist',urlencoded_parser,(req,res) => {
  mongoclient.connect(url, { useNewUrlParser: true },
    function(err, db) {
      if (err) throw err;
      var dbo = db.db("test");
      var year = parseInt(req.body.year);
      dbo.collection("category").aggregate([
        {$match:{"Year": year}},
        {$group:{_id:"$Category",Total:{$sum: "$Quantity"}}}
      ]).toArray(function(err, result) {
        if (err) throw err;
        console.log(result);
        res.send(result);

        db.close();
      });
    });
});

app.listen(8088);
console.log("Server listening on port 8088");
