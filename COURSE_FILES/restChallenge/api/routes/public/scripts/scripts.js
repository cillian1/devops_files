$(document).ready(function(){
  $.ajax({
    url: '/contacts',
    type: 'GET',
    success: function(response){
        var options = "";
        for(var i = 0;i < response.length; i++)
        {
            options += "<option value='" + response[i]._id + "'>" +
            response[i].name + "</option>";
        }
        $("#contacts").html(options);
    }
  });
});
