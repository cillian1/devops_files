const express = require('express'),
  bodyParser = require('body-parser'),
  _ = require('underscore'),
  json = require('./contacts.json')
  app = express();
  app.set('port',3500);
  app.use(bodyParser.urlencoded({extended:true}));
  app.use(bodyParser.json());
  let router = new express.Router();
  app.use('/',router);
  const server =  app.listen(app.get('port'),()=>{
    console.log('Server running on ' + app.get('port'));
  });
router.get('/',(req,res) => {
  res.json(json);
});
router.post('/',(req,res) => {
  if(req.body.id && req.body.name && req.body.phone){
    json.push(req.body);
    res.json(json);
  }
  else{
    res.json(500,{error:'Error inserting data'});
  }
});
router.put('/:id',(req,res) => {
  if(req.params.id && req.body.name && req.body.phone){
    _.each(json,(elem,index) => {
      if(elem.id = req.params.id){
        elem.name = req.body.name;
        elem.phone = req.body.phone;
      }
    });
    res.json(json);
  }
  else{
    res.json(500,{error:'Error updating data'});
  }
});

router.delete('/:id',(req, res) => {
  let indextoDEL = -1;
  _.each(json,(elem,index)=>{
    if(elem.id == req.params.id){
      indextoDEL = index;
    }
  });
  if(~indextoDEL){
    json.splice(indextoDEL,1);
  }
  res.json(json);
});
router.get('/new', (req,res) =>{
  res.sendFile(__dirname+ '/index.html')
});
