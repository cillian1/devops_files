const express = require('express');
const app = express();
app.set('view engine', 'ejs');
const body_parser = require('body-parser')
const urlencoded_parser = body_parser.urlencoded({ extended: true});
const mysql = require('mysql');
const con = mysql.createConnection({
  host: "localhost",
  user: 'root',
  password: "c0nygre",
  database: "world"
});
//SELECT name, population FROM city WHERE countrycode = 'IRL;'
con.connect(function(err) {
  if (err) throw err;
  console.log("Connected");
});


app.use(express.static('public'));

app.get('/',(req,res)=>{
  res.sendFile(__dirname + '/public/index.html');
});

app.get('/index.html',(req,res)=>{
  res.sendFile(__dirname + '/public/index.html');
});

app.get('/ajaxjs',(req,res)=>{
  res.sendFile(__dirname + '/public/ajaxjavascript.html');
});

app.get('/ajaxjq',(req,res)=>{
  res.sendFile(__dirname + '/public/ajaxjquery.html');
});

app.get('/echo',(req,res)=>{
  res.send(req.query.text.toUpperCase());
});

app.get('/city',(req,res)=>{
  res.sendFile(__dirname + '/public/citysearch.html');
});

app.get('/cityT',(req,res)=>{
  res.sendFile(__dirname + '/public/citytable.html');
});

app.get('/ajaxcity',(req,res)=>{
  res.sendFile(__dirname + '/public/citysearchajax.html');
});

app.get('/getcountrylistb',(req,res)=>{
	var sql = "SELECT name, code from country WHERE continent = ?";
	  con.query(sql,[req.query.continent],function (err, result) {
	    if (err) throw err;
			console.log(result);
			res.send(result);
	  });
});

app.get('/getcontinentlist',(req,res)=>{
	var sql = "SELECT DISTINCT continent from country";
	  con.query(sql,function (err, result) {
	    if (err) throw err;
			console.log(result);
			res.send(result);
	  });
});

app.get('/getcitylistaj',(req,res)=>{
	var sql = "SELECT id, name from city WHERE countrycode = ?";
	  con.query(sql,req.query.code, function (err, result) {
	    if (err) throw err;
			console.log(result);
			res.send(result);
	  });
});

app.get('/getcity',(req,res)=>{
	var sql = "SELECT id, name, population, district from city WHERE id = ?";
	  con.query(sql,req.query.id, function (err, result) {
	    if (err) throw err;
			console.log(result);
			res.send(result);
	  });
});

app.get('/findcities',(req,res)=>{
  if(req.query.start.length >=2){
    var cities = '';
  var sql = "SELECT c.name, countrycode, co.name country FROM city c JOIN country co on c.countrycode = co.code WHERE c.name LIKE ?;"


  //var sql = "SELECT name FROM city WHERE name LIKE ?";
  con.query(sql,[req.query.start + "%"],function (err, result) {
    if (err) throw err;
    console.log(result);
    for(var i=0; i<result.length; i++){
      cities += result[i].name + " "+ result[i].country + "<br>";
      //cities +="<td>" +result[i].name +"</td><td>"+results[i].country + "</td>";
    }



    res.send(cities);
  });
}else{
  res.send("");
}


});

app.get('/findcitiesT',(req,res)=>{
  if(req.query.start.length >=2){
    var cities = '';
  var sql = "SELECT c.name, countrycode, co.name country FROM city c JOIN country co on c.countrycode = co.code WHERE c.name LIKE ?;"
  con.query(sql,[req.query.start + "%"],function (err, result) {
    if (err) throw err;
    console.log(result);
    for(var i=0; i<result.length; i++){
      cities += "<table style='border: 1px solid black';><tr><td>"+result[i].name+"</td><td>"+result[i].country+"</td></tr></table>";
    }
    res.send(cities);
  });
}else{
  res.send("");
};
});

app.get('/displaycities',(req,res)=>{
var sql = "SELECT name, population FROM city WHERE countrycode = ?";
con.query(sql,[req.query.country],function(err,result){
  if(err) throw err;
  console.log(result);
  res.render('newtests.ejs',{cities:result});
  });
});

app.get('/countrycities',(req,res)=>{
  var sql = "SELECT name, code from country WHERE  continent = 'Europe'";
  con.query(sql,function(err,result){
    if(err)throw err;
    console.log(result);
    res.render('searchcities2.ejs',{cities:result});
  });
});

app.get('/countrycities',(req,res)=>{
  var sql = "SELECT name, code from country WHERE  continent = 'Europe'";
  con.query(sql,function(err,result){
    if(err)throw err;
    console.log(result);
    res.render('searchcities2.ejs',{cities:result});
  });
});



app.listen(8081);
console.log("Listening to port 8081");
